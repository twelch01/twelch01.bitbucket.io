/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Travis Welch Portfolio", "index.html", [
    [ "Mechatronics", "index.html", [
      [ "Introduction", "index.html#sec_intro", null ],
      [ "Lab Descriptions", "index.html#sec_lab", null ],
      [ "Final Project Ball Balancing Platform", "index.html#sec_labfin", null ]
    ] ],
    [ "Lab_0", "Lab_0.html", [
      [ "Lab 0 Fibonacci", "Lab_0.html#sec_lab0", null ]
    ] ],
    [ "Lab_1", "Lab_1.html", [
      [ "Lab 1 LED", "Lab_1.html#sec_lab1", null ]
    ] ],
    [ "Lab_2", "Lab_2.html", [
      [ "Lab 2 Encoder", "Lab_2.html#sec_lab2", null ]
    ] ],
    [ "Lab_3", "Lab_3.html", [
      [ "Lab 3 Motor", "Lab_3.html#sec_lab3", null ]
    ] ],
    [ "Lab_4", "Lab_4.html", [
      [ "Lab 4 Closedloop Controller", "Lab_4.html#sec_lab4", null ]
    ] ],
    [ "Lab_5", "Lab_5.html", [
      [ "Lab 5 Inertial Measurement Unit", "Lab_5.html#sec_lab5", null ]
    ] ],
    [ "Final_Project", "Final_Project.html", [
      [ "Final Term Project", "Final_Project.html#sec_lab6", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"DRV8847_8py.html",
"classimu__driver_1_1IMU__driver.html#af0ca9d3a8d583d817f34623a4ad553f3",
"classtask__user3_1_1Task__user.html#a11d8b826e15f60d470c98e83cef05843",
"main6_8py.html#a331567f57d52247f96523a3e6c09b20a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';