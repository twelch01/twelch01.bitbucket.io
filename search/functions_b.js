var searchData=
[
  ['save_5for_5fstore_5fcalibration_0',['save_or_store_calibration',['../classtouch__panel__task_1_1Touch__Panel__Task.html#a4d5763882b281d926333f703553ebae7',1,'touch_panel_task::Touch_Panel_Task']]],
  ['set_5fduty_1',['set_duty',['../classDRV8847_1_1Motor.html#ae2e6c0feeb46de3f93c35e7f25a79a8b',1,'DRV8847.Motor.set_duty()'],['../classDRV8847__6_1_1Motor.html#acf917819dfa6ea180cd86e64c42bac73',1,'DRV8847_6.Motor.set_duty()']]],
  ['set_5fimu_5fcal_5fcoef_2',['set_IMU_Cal_Coef',['../classimu__driver_1_1IMU__driver.html#adbc353ba718d50f35b16393d58716533',1,'imu_driver.IMU_driver.set_IMU_Cal_Coef()'],['../classimu__driver__6_1_1IMU__driver.html#a7c6b6f121878d8af281afcaa690f6ad1',1,'imu_driver_6.IMU_driver.set_IMU_Cal_Coef()']]],
  ['set_5fkp_3',['set_Kp',['../classclosedloop_1_1Closedloop.html#a4d71eaadbb7f1f3cb6ccb29bb8ca9040',1,'closedloop::Closedloop']]],
  ['set_5fposition_4',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]]
];
