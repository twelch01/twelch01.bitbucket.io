var searchData=
[
  ['x_0',['x',['../classdata__collection__task_1_1Data__Collection__Task.html#ac7c324055a181679dde1c28b1186d9b6',1,'data_collection_task.Data_Collection_Task.x()'],['../classpanel__driver_1_1Panel__driver.html#aa3640c16641bff3907bfca5815f573c4',1,'panel_driver.Panel_driver.x()'],['../classtouch__panel__task_1_1Touch__Panel__Task.html#aeb771efd9c4ae2498601c0b65d192c02',1,'touch_panel_task.Touch_Panel_Task.x()'],['../main6_8py.html#aa2e4ac67d675fd32531ef5c9649572e1',1,'main6.x()']]],
  ['x_5fbotleft_1',['x_BotLeft',['../classpanel__driver_1_1Panel__driver.html#a723499a80812f8803bdf829c8b50d87f',1,'panel_driver::Panel_driver']]],
  ['x_5fbotright_2',['x_BotRight',['../classpanel__driver_1_1Panel__driver.html#a5a08af15cb6d3278b64d8e321036694d',1,'panel_driver::Panel_driver']]],
  ['x_5fc_3',['x_c',['../classpanel__driver_1_1Panel__driver.html#a9440ac4592976ed4e1e7e56730e0e9a9',1,'panel_driver::Panel_driver']]],
  ['x_5fhat_4',['x_hat',['../classpanel__driver_1_1Panel__driver.html#af6568f84cbde85ae67c54888c56d6252',1,'panel_driver::Panel_driver']]],
  ['x_5ftopleft_5',['x_TopLeft',['../classpanel__driver_1_1Panel__driver.html#abd901cc64064014492f6dac834375ece',1,'panel_driver::Panel_driver']]],
  ['x_5ftopright_6',['x_TopRight',['../classpanel__driver_1_1Panel__driver.html#a4f09bd1f08c8cc884aea0e5d24545b8a',1,'panel_driver::Panel_driver']]],
  ['xdot_7',['xdot',['../classdata__collection__task_1_1Data__Collection__Task.html#a12c0ef0f0c89c25f25037d4afb2f4054',1,'data_collection_task.Data_Collection_Task.xdot()'],['../classtouch__panel__task_1_1Touch__Panel__Task.html#aadb9f777280d8f0d5faa1ba1309547da',1,'touch_panel_task.Touch_Panel_Task.xdot()'],['../main6_8py.html#a74a4aeae66a014f44505d95dae26fdc8',1,'main6.xdot()']]],
  ['xm_8',['xm',['../classpanel__driver_1_1Panel__driver.html#a7c6355478a7175034438be547e30d02a',1,'panel_driver::Panel_driver']]],
  ['xp_9',['xp',['../classpanel__driver_1_1Panel__driver.html#a8980e3216d0011d007d57241c4350311',1,'panel_driver::Panel_driver']]]
];
