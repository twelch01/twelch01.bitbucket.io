var searchData=
[
  ['r_0',['R',['../classclosedloop__6_1_1Closedloop.html#acf846edd54dca06e3764ec1aafa3c19e',1,'closedloop_6::Closedloop']]],
  ['ref_5fdelta_1',['ref_delta',['../classtask__controller_1_1Task__controller.html#a6aa9e27574cf5e443493189f05ec5ee2',1,'task_controller::Task_controller']]],
  ['roll_2',['Roll',['../classimu__driver_1_1IMU__driver.html#a11ab11e7217154f6744f95e25cffd0f5',1,'imu_driver.IMU_driver.Roll()'],['../classimu__driver__6_1_1IMU__driver.html#a78cff4aa3ae099e20735d7cd8c010ef1',1,'imu_driver_6.IMU_driver.Roll()']]],
  ['roll_3',['roll',['../classtask__imu_1_1Task__imu.html#a197249afe0a97a364169932c1b980a42',1,'task_imu.Task_imu.roll()'],['../main6_8py.html#af805ce75108234caaa8ebacc125660be',1,'main6.roll()']]],
  ['run_4',['run',['../lab1_8py.html#a306152090654ec976ed1f6f53c463718',1,'lab1']]],
  ['runs_5',['runs',['../classtask__user_1_1Task__user.html#acd05b7d13c21c95e3f4e55d547cdf083',1,'task_user.Task_user.runs()'],['../classtask__user3_1_1Task__user.html#aeffa2ae8fa9c0d209180e700d2d6242f',1,'task_user3.Task_user.runs()'],['../classtask__user4_1_1Task__user.html#a483593cd884ee65b7022788f2428a523',1,'task_user4.Task_user.runs()'],['../classtask__user6_1_1Task__user.html#a8e56f6b47b63b6ee965478ee27080eed',1,'task_user6.Task_user.runs()']]]
];
