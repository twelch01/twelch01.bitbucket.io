var searchData=
[
  ['new_5fdelta_0',['new_delta',['../classencoder_1_1Encoder.html#a43f581b07c7d05543e88c4d2239db7d5',1,'encoder::Encoder']]],
  ['new_5ftick_1',['new_tick',['../classencoder_1_1Encoder.html#a022f36c023c2a4214e10296bc125d212',1,'encoder::Encoder']]],
  ['newstate_2',['newstate',['../classclosedloop__6_1_1Closedloop.html#a151b5286d84db89b6ff3748d72c65619',1,'closedloop_6::Closedloop']]],
  ['next_5ftime_3',['next_time',['../classtask__encoder__file_1_1Task__encoder.html#a1f5505dcf779c332e2218cf5749bae8e',1,'task_encoder_file::Task_encoder']]],
  ['nexttime_4',['nextTime',['../classdata__collection__task_1_1Data__Collection__Task.html#ad454589cd45b8af3f04c5a0011396e0b',1,'data_collection_task.Data_Collection_Task.nextTime()'],['../classtask__user3_1_1Task__user.html#a7467e19d68a7c998869af83673a7bcc6',1,'task_user3.Task_user.nextTime()'],['../classtask__user4_1_1Task__user.html#a180706b8dc5455a740b5d9e49a19b326',1,'task_user4.Task_user.nextTime()']]],
  ['nfault_5',['nFault',['../classDRV8847_1_1DRV8847.html#a508c2adc1f6b9e0f2d5a2c53e27ba1ce',1,'DRV8847.DRV8847.nFault()'],['../classDRV8847__6_1_1DRV8847.html#acf9fd2549c9acee0f225bb6694482915',1,'DRV8847_6.DRV8847.nFault()']]],
  ['nsleep_6',['nSleep',['../classDRV8847_1_1DRV8847.html#a65ffd34ffbd3e98365f8b49c5cf423b3',1,'DRV8847.DRV8847.nSleep()'],['../classDRV8847__6_1_1DRV8847.html#acb7f41d50377c31e01c2f3bb5b9cb358',1,'DRV8847_6.DRV8847.nSleep()']]]
];
