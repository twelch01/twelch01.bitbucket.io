var namespaces_dup =
[
    [ "closedloop", null, [
      [ "Closedloop", "classclosedloop_1_1Closedloop.html", "classclosedloop_1_1Closedloop" ]
    ] ],
    [ "closedloop_6", null, [
      [ "Closedloop", "classclosedloop__6_1_1Closedloop.html", "classclosedloop__6_1_1Closedloop" ]
    ] ],
    [ "data_collection_task", null, [
      [ "Data_Collection_Task", "classdata__collection__task_1_1Data__Collection__Task.html", "classdata__collection__task_1_1Data__Collection__Task" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV8847_6", null, [
      [ "DRV8847", "classDRV8847__6_1_1DRV8847.html", "classDRV8847__6_1_1DRV8847" ],
      [ "Motor", "classDRV8847__6_1_1Motor.html", "classDRV8847__6_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "imu_driver", null, [
      [ "IMU_driver", "classimu__driver_1_1IMU__driver.html", "classimu__driver_1_1IMU__driver" ]
    ] ],
    [ "imu_driver_6", null, [
      [ "IMU_driver", "classimu__driver__6_1_1IMU__driver.html", "classimu__driver__6_1_1IMU__driver" ]
    ] ],
    [ "Lab0Fibonacci", null, [
      [ "fib", "Lab0Fibonacci_8py.html#af5482ec2bd8d105f9c5629e3afae248f", null ]
    ] ],
    [ "lab1", null, [
      [ "onButtonPressFCN", "lab1_8py.html#a58c80dd8232c88af9b415188fe010b71", null ],
      [ "reset_timer", "lab1_8py.html#ac48e03ff516187da48d139247766df47", null ],
      [ "update_SQW", "lab1_8py.html#a7f7078ce95e80bdd0394d86aed8646d8", null ],
      [ "update_STW", "lab1_8py.html#ab6e01d88cd674869439b1185cba5b130", null ],
      [ "update_SW", "lab1_8py.html#a237c4ab0231447da2c8952c924602cd4", null ],
      [ "update_timer", "lab1_8py.html#a9411d3a74e191bddf5cd77a8ed77ce1d", null ],
      [ "ButtonInt", "lab1_8py.html#a17e455b2b0975b609f1769b6658eddde", null ],
      [ "pinA5", "lab1_8py.html#ae17f7efc37648b119e8532f40dda69f1", null ],
      [ "pinC13", "lab1_8py.html#a44f915dbd667a2aeffd7d4cd432270af", null ],
      [ "run", "lab1_8py.html#a306152090654ec976ed1f6f53c463718", null ],
      [ "state", "lab1_8py.html#a40c9aae6659688d7bf23acc6ef902f67", null ],
      [ "time", "lab1_8py.html#a91cede1c70ef0f9524b7c824785573d0", null ]
    ] ],
    [ "main6", null, [
      [ "Ball_Balanced", "main6_8py.html#af13e56ca87afc67da6d2150ceb4f1015", null ],
      [ "duty_1", "main6_8py.html#a195ef22c7ec80fc08da64fedf366da02", null ],
      [ "duty_2", "main6_8py.html#a947163ecd43dd59806b29db4c31b46a4", null ],
      [ "enable", "main6_8py.html#ab4fe6e905e762f59bd858635e00357be", null ],
      [ "heading", "main6_8py.html#a77968c2f7945fe9c7488bc08927fd0af", null ],
      [ "K_1", "main6_8py.html#a2c9d7ba13546ce829fd0dc481984361c", null ],
      [ "k_1", "main6_8py.html#a7857e5c58fb25b8748a361a3007bb63d", null ],
      [ "K_2", "main6_8py.html#a54efd111a7898dc8f4aa626c3c8567da", null ],
      [ "k_2", "main6_8py.html#aa015e3c774aae3da0a5235987b2a2230", null ],
      [ "motor_1", "main6_8py.html#a3896ff0b4d59c9e493b650c1bb9002a3", null ],
      [ "motor_2", "main6_8py.html#a7687b8b303e3f6a1d1c0ec8e638b0430", null ],
      [ "motor_drv", "main6_8py.html#a8a0e6bb9810957cf6c077fb7ab583bed", null ],
      [ "period", "main6_8py.html#a32bd4541e9f790434bf7c2e56434b210", null ],
      [ "pitch", "main6_8py.html#a539e9b382316ededdd8deab513e65130", null ],
      [ "printData", "main6_8py.html#a7071a4aa41a64ff3003b0400f43b0cde", null ],
      [ "roll", "main6_8py.html#af805ce75108234caaa8ebacc125660be", null ],
      [ "state", "main6_8py.html#a6292e759d483abf32aed74ef7f9c3400", null ],
      [ "task_controller_1", "main6_8py.html#a2d60b69c3b1d448139e344d1eefa84ba", null ],
      [ "task_controller_2", "main6_8py.html#a68a31a51d1bfecd389bb5f1c520423b9", null ],
      [ "task_data", "main6_8py.html#adbd590457fe3eff2e96e4c36eaf8a948", null ],
      [ "task_imu", "main6_8py.html#a4a78b4d1b6344c334731a8ac2358d926", null ],
      [ "task_motor_1", "main6_8py.html#a331567f57d52247f96523a3e6c09b20a", null ],
      [ "task_motor_2", "main6_8py.html#a89c91cbe975ebe456384046d0c77bbe5", null ],
      [ "task_panel", "main6_8py.html#abccad0934822baab8e7b810550912d9a", null ],
      [ "task_user", "main6_8py.html#ad7750a8db4a3536084e19a39c1fa1004", null ],
      [ "wx", "main6_8py.html#a2d5550fade3ec51325b5595c507bcf8b", null ],
      [ "wy", "main6_8py.html#a8b341b9aeca2fc5a92d6a57db8b43c66", null ],
      [ "wz", "main6_8py.html#a71b83b089e3f40990f8a46691c4e9adc", null ],
      [ "x", "main6_8py.html#aa2e4ac67d675fd32531ef5c9649572e1", null ],
      [ "xdot", "main6_8py.html#a74a4aeae66a014f44505d95dae26fdc8", null ],
      [ "y", "main6_8py.html#ad1911609b5da47bf2f38dcd1b4962594", null ],
      [ "ydot", "main6_8py.html#a552cca84b94be7c163836c600d8e0eff", null ]
    ] ],
    [ "main_lab2", null, [
      [ "idx", "main__lab2_8py.html#af1d6586764af5bc3a15fa8ed939f3307", null ],
      [ "task_encoder_1", "main__lab2_8py.html#aafe37c76e86bfd24d62a401b7e7ce1f3", null ],
      [ "task_encoder_2", "main__lab2_8py.html#ae59c2df0fc5ed53e0a7cc83a93a4c281", null ],
      [ "task_user", "main__lab2_8py.html#a360535e9857fe201b83bc822bf0b43df", null ]
    ] ],
    [ "main_lab3", null, [
      [ "delta_1", "main__lab3_8py.html#a1b53cf6015ac0a299056bd46f0b872d0", null ],
      [ "delta_2", "main__lab3_8py.html#a26a376e36c5f4dd9aa40b4863236bc2a", null ],
      [ "duty_1", "main__lab3_8py.html#aa0434073ea90ccf763f894bbf8129d47", null ],
      [ "duty_2", "main__lab3_8py.html#a3c6d3407731b7d3dc3a71be2ddd3c091", null ],
      [ "enable", "main__lab3_8py.html#a981c758e828b5adfa2c041fb81cec2b4", null ],
      [ "list_1", "main__lab3_8py.html#af83195d8dc2cc7cb9002705509ddfe6e", null ],
      [ "list_2", "main__lab3_8py.html#aa7c6ba4192d86c4a9b25b45ac84b1cb4", null ],
      [ "motor1_data", "main__lab3_8py.html#ac8b535ebe2cc4895db90bc59fd0e01ae", null ],
      [ "motor2_data", "main__lab3_8py.html#a09c86c2ed6d26f0de7ad1ca58a95aa52", null ],
      [ "motor_1", "main__lab3_8py.html#a8e4166c986de46cedbc9c97f75f79c90", null ],
      [ "motor_2", "main__lab3_8py.html#aa0ebe0d9210242f992a905cfdb52679a", null ],
      [ "motor_drv", "main__lab3_8py.html#a387fd0336c60ee58778997a59f3136c6", null ],
      [ "motor_task_1", "main__lab3_8py.html#a6ed8c12d9901ec5cb633b216ab5c0ede", null ],
      [ "motor_task_2", "main__lab3_8py.html#ac73f28c5d97f2df6fc4894667719655a", null ],
      [ "position_1", "main__lab3_8py.html#aa3df54639aa4fcf7a4a25a4fd87521f1", null ],
      [ "position_2", "main__lab3_8py.html#a87ec10c12a3eade072050c35ae07d7d5", null ],
      [ "setZero_1", "main__lab3_8py.html#a8c105a2837586f416961216b4352baeb", null ],
      [ "setZero_2", "main__lab3_8py.html#a667fdeefea6e92060190719a2ff0c0df", null ],
      [ "task_encoder_1", "main__lab3_8py.html#aba10ea9b19f4144cd8cd5d19ab5e2e1d", null ],
      [ "task_encoder_2", "main__lab3_8py.html#a710155b62951b5ebfca6a579711b8ec5", null ],
      [ "task_user", "main__lab3_8py.html#a10fab642243496a9a65643c00d177bc5", null ]
    ] ],
    [ "main_lab4", null, [
      [ "delta_1", "main__lab4_8py.html#a610343fa63459b9a9c07ab0b339046c6", null ],
      [ "delta_2", "main__lab4_8py.html#a960e014761a588f11bcaf9fabbe47fa1", null ],
      [ "duty_1", "main__lab4_8py.html#a2f336a3ede750b2cec98d6d4e6400e75", null ],
      [ "duty_2", "main__lab4_8py.html#ac28457186e9b7843a47179daa5c07c21", null ],
      [ "enable", "main__lab4_8py.html#af177a8e9755285985f2e2e688d58043c", null ],
      [ "encoder_period", "main__lab4_8py.html#ae63d9083a01e76fd36b20b32866bde33", null ],
      [ "input_delta1", "main__lab4_8py.html#ac65abb7bf91c515c21dff92c888198c5", null ],
      [ "input_delta2", "main__lab4_8py.html#a6112de0c2f7fa00f1c3a36fb271ef972", null ],
      [ "Kp_1", "main__lab4_8py.html#afba56061383a4fbc759476ec303d2e2c", null ],
      [ "Kp_2", "main__lab4_8py.html#aecbee33fd9f879f1bd99007bd74895bd", null ],
      [ "list_1", "main__lab4_8py.html#a83545b74ad98d046021afd593a1b7c2f", null ],
      [ "list_2", "main__lab4_8py.html#afc4e03f56c02bd021007e6f437ffab68", null ],
      [ "motor1_data", "main__lab4_8py.html#a1cdf452ff057f9c59923a19e02636393", null ],
      [ "motor2_data", "main__lab4_8py.html#a45ef34c704329765f2ac374dd1aec018", null ],
      [ "motor_1", "main__lab4_8py.html#a173586ffa5fde7d064254dee59c36dbd", null ],
      [ "motor_2", "main__lab4_8py.html#a80e9f5e44dc757b69ee45104db9318d7", null ],
      [ "motor_drv", "main__lab4_8py.html#a60cd91d7110619a9e6d84fe19f4c5910", null ],
      [ "motor_task_1", "main__lab4_8py.html#a1e99b21b9f6f2591c41425184f121528", null ],
      [ "motor_task_2", "main__lab4_8py.html#a1e049031c4da57191a7a65e54819621b", null ],
      [ "position_1", "main__lab4_8py.html#afa35d19c9a50013ef50cb7ca7ae42811", null ],
      [ "position_2", "main__lab4_8py.html#a8e0c512a0cfc7bc1d692350a0dec79ac", null ],
      [ "setZero_1", "main__lab4_8py.html#a5f4a9710e5abfed2310b659c7eb35f1f", null ],
      [ "setZero_2", "main__lab4_8py.html#a584481d44cfe2b4e7cb88fb1fcaed981", null ],
      [ "task_controller_1", "main__lab4_8py.html#ab8433cba1955f4476946f2ff5e1c9f24", null ],
      [ "task_controller_2", "main__lab4_8py.html#a9e05e3268159618d63436e92d7b8b590", null ],
      [ "task_encoder_1", "main__lab4_8py.html#aa5ac993927689df8d711f58192e36009", null ],
      [ "task_encoder_2", "main__lab4_8py.html#a429f94a71b2d1615cac47cf0c52e6690", null ],
      [ "task_user", "main__lab4_8py.html#ad07c4c6d8fa19f607915fbf8717d40c0", null ]
    ] ],
    [ "main_lab5", null, [
      [ "cal_status", "main__lab5_8py.html#a010fd43b4dd04a45b02865a7a97adae1", null ],
      [ "heading", "main__lab5_8py.html#ae3874b6967c2b43765e39a6c932cddce", null ],
      [ "i2c", "main__lab5_8py.html#a4ecdc4452e37e47c31ac5a00f0273967", null ],
      [ "imu", "main__lab5_8py.html#a5253b73a26e8440f638cb268578a8cde", null ],
      [ "wx", "main__lab5_8py.html#a48a84003322e14971de2e4daf658a39d", null ]
    ] ],
    [ "motor_task", null, [
      [ "Task_motor", "classmotor__task_1_1Task__motor.html", "classmotor__task_1_1Task__motor" ]
    ] ],
    [ "motor_task_6", null, [
      [ "Task_motor", "classmotor__task__6_1_1Task__motor.html", "classmotor__task__6_1_1Task__motor" ]
    ] ],
    [ "panel_driver", null, [
      [ "Panel_driver", "classpanel__driver_1_1Panel__driver.html", "classpanel__driver_1_1Panel__driver" ]
    ] ],
    [ "share", null, [
      [ "Queue", "classshare_1_1Queue.html", "classshare_1_1Queue" ],
      [ "Share", "classshare_1_1Share.html", "classshare_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_controller", "classtask__controller_1_1Task__controller.html", "classtask__controller_1_1Task__controller" ]
    ] ],
    [ "task_controller_6", null, [
      [ "Task_controller", "classtask__controller__6_1_1Task__controller.html", "classtask__controller__6_1_1Task__controller" ]
    ] ],
    [ "task_encoder_file", null, [
      [ "Task_encoder", "classtask__encoder__file_1_1Task__encoder.html", "classtask__encoder__file_1_1Task__encoder" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_imu", "classtask__imu_1_1Task__imu.html", "classtask__imu_1_1Task__imu" ]
    ] ],
    [ "task_user", null, [
      [ "Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ],
    [ "task_user3", null, [
      [ "Task_user", "classtask__user3_1_1Task__user.html", "classtask__user3_1_1Task__user" ]
    ] ],
    [ "task_user4", null, [
      [ "Task_user", "classtask__user4_1_1Task__user.html", "classtask__user4_1_1Task__user" ]
    ] ],
    [ "task_user6", null, [
      [ "Task_user", "classtask__user6_1_1Task__user.html", "classtask__user6_1_1Task__user" ]
    ] ],
    [ "touch_panel_task", null, [
      [ "Touch_Panel_Task", "classtouch__panel__task_1_1Touch__Panel__Task.html", "classtouch__panel__task_1_1Touch__Panel__Task" ]
    ] ]
];