var main__lab3_8py =
[
    [ "delta_1", "main__lab3_8py.html#a1b53cf6015ac0a299056bd46f0b872d0", null ],
    [ "delta_2", "main__lab3_8py.html#a26a376e36c5f4dd9aa40b4863236bc2a", null ],
    [ "duty_1", "main__lab3_8py.html#aa0434073ea90ccf763f894bbf8129d47", null ],
    [ "duty_2", "main__lab3_8py.html#a3c6d3407731b7d3dc3a71be2ddd3c091", null ],
    [ "enable", "main__lab3_8py.html#a981c758e828b5adfa2c041fb81cec2b4", null ],
    [ "list_1", "main__lab3_8py.html#af83195d8dc2cc7cb9002705509ddfe6e", null ],
    [ "list_2", "main__lab3_8py.html#aa7c6ba4192d86c4a9b25b45ac84b1cb4", null ],
    [ "motor1_data", "main__lab3_8py.html#ac8b535ebe2cc4895db90bc59fd0e01ae", null ],
    [ "motor2_data", "main__lab3_8py.html#a09c86c2ed6d26f0de7ad1ca58a95aa52", null ],
    [ "motor_1", "main__lab3_8py.html#a8e4166c986de46cedbc9c97f75f79c90", null ],
    [ "motor_2", "main__lab3_8py.html#aa0ebe0d9210242f992a905cfdb52679a", null ],
    [ "motor_drv", "main__lab3_8py.html#a387fd0336c60ee58778997a59f3136c6", null ],
    [ "motor_task_1", "main__lab3_8py.html#a6ed8c12d9901ec5cb633b216ab5c0ede", null ],
    [ "motor_task_2", "main__lab3_8py.html#ac73f28c5d97f2df6fc4894667719655a", null ],
    [ "position_1", "main__lab3_8py.html#aa3df54639aa4fcf7a4a25a4fd87521f1", null ],
    [ "position_2", "main__lab3_8py.html#a87ec10c12a3eade072050c35ae07d7d5", null ],
    [ "setZero_1", "main__lab3_8py.html#a8c105a2837586f416961216b4352baeb", null ],
    [ "setZero_2", "main__lab3_8py.html#a667fdeefea6e92060190719a2ff0c0df", null ],
    [ "task_encoder_1", "main__lab3_8py.html#aba10ea9b19f4144cd8cd5d19ab5e2e1d", null ],
    [ "task_encoder_2", "main__lab3_8py.html#a710155b62951b5ebfca6a579711b8ec5", null ],
    [ "task_user", "main__lab3_8py.html#a10fab642243496a9a65643c00d177bc5", null ]
];